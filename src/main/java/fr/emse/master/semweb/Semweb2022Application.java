package fr.emse.master.semweb;


import net.fortuna.ical4j.data.ParserException;
import org.apache.commons.io.FileUtils;
import org.apache.jena.base.Sys;
import org.apache.jena.graph.Graph;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.LiteralImpl;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shacl.ShaclValidationException;
import org.apache.jena.shacl.ShaclValidator;
import org.apache.jena.shacl.Shapes;
import org.apache.jena.shacl.ValidationReport;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;
import java.net.URISyntaxException;
import java.net.http.HttpHeaders;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@SpringBootApplication
@Controller
public class Semweb2022Application {

	HttpQueryManager httpQueryManager;
	FileManager fileManager;
	RDFManager rdfManager;

	public Semweb2022Application() {
		this.httpQueryManager = new HttpQueryManager();
		this.fileManager = new FileManager();
		this.rdfManager = new RDFManager();
	}

	public static void main(String[] args) {
		SpringApplication.run(Semweb2022Application.class, args);
	}

	@GetMapping(value = { "/parsing" })
	public String indexParsing(Model model) {

		return "parsing";
	}

	@PostMapping("/upload")
	public String handleFileUpload(@RequestParam("icalFile") MultipartFile file,
								   RedirectAttributes redirectAttributes) throws ParserException, IOException, ParseException, URISyntaxException, InterruptedException {

		// check if file is empty
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}

		try {
			// Create the "uploaded_files" directory if it does not exist
			Path directory = Paths.get("src/main/resources/uploaded_files");
			if (!Files.exists(directory)) {
				Files.createDirectory(directory);
			}

			// Get the file and save it to the "uploaded_files" directory
			byte[] bytes = file.getBytes();
			Path path = Paths.get("src/main/resources/uploaded_files/" + file.getOriginalFilename());
			Files.write(path, bytes);

		}  catch (IOException e) {
			redirectAttributes.addFlashAttribute("error", "Error uploading file: " + e.getMessage());
			return "redirect:/uploadStatus";
		}

		int ok = RDFparser.parseAndUpload("src/main/resources/uploaded_files/" + file.getOriginalFilename());
		if (ok == -1) {
			redirectAttributes.addFlashAttribute("message", "ICAL file is not valid");
			return "redirect:/uploadStatus";
		}
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded '" + file.getOriginalFilename() + "'");
		return "redirect:/uploadStatus";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

	@GetMapping(value = { "/" })
	public String index(Model model) {
		try {
			HttpResponse<String> response = httpQueryManager.httpQueryPostSparqlQuery(
					"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
					fileManager.getDataFilepath("selectAllTriples.rq"));
			model.addAttribute("triples", rdfManager.getTriples(response));
			List<List<String>> events = rdfManager.getAllEvents(
					httpQueryManager.httpQueryPostSparqlQuery(
							"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
							fileManager.getDataFilepath("allEvents.rq")
					));
			model.addAttribute("events", events);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "main";
	}

	@GetMapping(value = { "/myfuturecourses" })
	public String myFutureCourses(Model model) throws URISyntaxException, IOException, InterruptedException {
		List<List<String>> events = rdfManager.getAllEvents(
				httpQueryManager.httpQueryPostSparqlQuery(
						"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
						fileManager.getDataFilepath("myFutureCourses.rq")
				));
		model.addAttribute("triples", events);
		return "myFutureCourses";
	}

	@GetMapping(value = { "/steEvents" })
	public String saintEtienneEvents(Model model) throws URISyntaxException, IOException, InterruptedException {
		List<List<String>> events = rdfManager.getAllEvents(
				httpQueryManager.httpQueryPostSparqlQuery(
						"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
						fileManager.getDataFilepath("steEvents.rq")
				));
		List<List<String>> events2 = rdfManager.getAllEvents(
				httpQueryManager.httpQueryPostSparqlQuery(
						"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
						fileManager.getDataFilepath("newSteEvents.rq")
				));

		model.addAttribute("events", events2);
		model.addAttribute("triples", events);
		return "steEvents";
	}


	@GetMapping(value = { "/add" })
	public String addEvent(Model model) {
		Event event = new Event();
		event.setName("default name");
		event.setLocation("default location");
		event.setStartDate(LocalDateTime.now());
		event.setEndDate(LocalDateTime.now().plusDays(1));
		model.addAttribute("event", event);
		return "add";
	}

	@GetMapping(value = { "/event/modify/{iri}"})
	public String modifyEvent(Model model, @PathVariable(value="iri") String eventIRI)
			throws URISyntaxException, IOException, InterruptedException {
		// On fait une requete GET au bon URI pour avoir le ttl de l'event, on le parse avec Jena,
		// on obient une resource, on en crée un Event et on le passe au modèle
		String base = "https://territoire.emse.fr/ldp/adamelbesriromanguirbal/";
		String iri = base + eventIRI + "/";
//		HttpResponse<String> response = httpQueryManager.httpQueryGet(iri);

		model.addAttribute("iri", iri);

//		File tmpFile = File.createTempFile("semweb-2022-", ".tmp");
//		org.apache.jena.rdf.model.Model rdfModel = ModelFactory.createDefaultModel();
//		InputStream in = RDFDataMgr.open(tmpFile.getPath());
//		if (in == null) {
//			throw new IllegalArgumentException("File: " + tmpFile + " not found");
//		}
//		rdfModel.read(in, null, "text/turtle");
//		Resource eventResource = rdfModel.getResource(iri);
//		eventResource.addProperty(rdfModel.getProperty("http://schema.org/attendee"), attendeeName);

		return "addAttendeeForm";
	}

	@PostMapping("/addAttendee")
	public String addAttendee(RedirectAttributes redirectAttributes, @RequestParam("iri") String iri, @RequestParam("attendee") String attendeeName) {
		try {
			HttpResponse<String> response = httpQueryManager.httpQueryGet(iri);
			StringReader reader = new StringReader(response.body());
			org.apache.jena.rdf.model.Model rdfModel = ModelFactory.createDefaultModel();
			rdfModel.read(reader, null, "text/turtle");

//			rdfModel.addLiteral(
//					rdfModel.getResource(iri),
//					rdfModel.getProperty("http://schema.org/attendee"),
//					ResourceFactory.createStringLiteral(attendeeName));

			System.out.print("ORIGINAL EVENT : ");
			rdfModel.write(System.out, "TTL");

//			org.apache.jena.rdf.model.Model rdfModel = ModelFactory.createDefaultModel();
//			rdfModel = rdfModel.addLiteral(
//					rdfModel.createResource(iri),
//					rdfModel.createProperty("http://schema.org/attendee"),
//					rdfModel.createLiteral(attendeeName));

			Resource attendeeNode = rdfModel.createResource().addProperty(rdfModel.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdfModel.createResource("http://schema.org/Person"))
					.addProperty(rdfModel.createProperty("http://schema.org/name"), attendeeName);
			rdfModel.getResource(iri).addProperty(rdfModel.createProperty("http://schema.org/attendee"), attendeeNode);

//			Property hasPredicate = rdfModel.getProperty("http://www.w3.org/ns/ldp#");
//			Selector selector = new SimpleSelector(null, hasPredicate, (RDFNode) null);
//			StmtIterator iter = rdfModel.listStatements(selector);
//			while(iter.hasNext()) {
//				Statement stmt = iter.nextStatement();
//				rdfModel.remove(stmt);
//			}
//			hasPredicate = rdfModel.getProperty("<https://carbonldp.com/ns/");
//			selector = new SimpleSelector(null, hasPredicate, (RDFNode) null);
//			iter = rdfModel.listStatements(selector);
//			while(iter.hasNext()) {
//				Statement stmt = iter.nextStatement();
//				rdfModel.remove(stmt);
//			}

			String carbonLDPNS = "https://carbonldp.com/ns/v1/platform#";
			String ldpNS = "http://www.w3.org/ns/ldp#";

			StmtIterator iter = rdfModel.listStatements();
			while (iter.hasNext()) {
				Statement stmt = iter.next();
				Property predicate = stmt.getPredicate();
				String predicateNS = predicate.getNameSpace();

				// Check if the predicate is in the carbon LDP namespace
				if (predicateNS.equals(carbonLDPNS) || predicateNS.equals(ldpNS)) {
					// Remove the triple from the model
					iter.remove();
				}
			}

			Writer writer = new StringWriter();
			rdfModel.write(writer, "TTL");
			String modifiedEvent = writer.toString();

			System.out.println("MODIFIED EVENT : "+modifiedEvent);

			httpQueryManager.httpQueryDelete(iri);

//			HttpResponse<String> response = httpQueryManager.httpQueryGet(iri);
//			HttpHeaders headers = response.headers();
//			String etag = String.valueOf(headers.firstValue("ETag")).replaceAll("[^\\d\\-]", "");
//			System.out.println(etag);

//			HttpResponse<String> postResponse = httpQueryManager
//					.httpQueryPutRawData(
//							iri,
//							modifiedEvent,
//							etag);
			HttpResponse<String> postResponse = httpQueryManager
					.httpQueryPostRawData(
							"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
							modifiedEvent);

			httpQueryManager.treatResponseCode(postResponse, redirectAttributes);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			redirectAttributes.addFlashAttribute("error", "Failed to add an attendee");
		}
		return "redirect:/";
	}

	@PostMapping("/event")
	public String createEvent(@ModelAttribute Event event, Model model, RedirectAttributes redirectAttributes)
			throws URISyntaxException, IOException, InterruptedException {
		// Do something with the event object, such as saving it to a database
		// ...
		RDF rdfCreator = new RDF();
		String fileSuffix = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String res =  rdfCreator.createRDF(event.getAttendee(), event.getIsCourse(),fileSuffix, event.getName(), event.getStartDate(), event.getEndDate(), event.getLocation(), event.getDescription());

		// Validate with SHACL
		Graph dataGraph = RDFDataMgr.loadGraph(res);
		Shapes shapes = Shapes.parse(fileManager.getDataFilepath("shapes.ttl"));
		ValidationReport validationReport = ShaclValidator.get().validate(shapes, dataGraph);

		try {
			HttpResponse<String> response;
			if (validationReport.conforms()) {
				response = httpQueryManager.httpQueryPostData(
						"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
						res);
				httpQueryManager.treatResponseCode(response, redirectAttributes);
			} else {
				throw new ShaclValidationException(validationReport);
			}
		} catch (ShaclValidationException e) {
			rdfManager.treatShaclException(redirectAttributes, e.getReport());
			return "redirect:/";
		}

//		System.out.println(response);
		model.addAttribute("event", event);
		return "event-details";
	}



	@GetMapping(value = "/territoire-event")
	public String TerritoireLDPTestEvent(RedirectAttributes redirAttrs,
									@RequestParam(required = false, defaultValue = "false") String delete) {
		try {
			HttpResponse<String> response;
			if (Objects.equals(delete, "true")) {
				response = httpQueryManager.httpQueryDelete(
						"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/eventdetest/");
			} else {
				Graph dataGraph = RDFDataMgr.loadGraph(fileManager.getDataFilepath("event_test.ttl"));
				Shapes shapes = Shapes.parse(fileManager.getDataFilepath("shapes.ttl"));
				ValidationReport validationReport = ShaclValidator.get().validate(shapes, dataGraph);

				if (validationReport.conforms()) {
					response = httpQueryManager.httpQueryPostData(
							"https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
							fileManager.getDataFilepath("event_test.ttl"));
				} else {
					throw new ShaclValidationException(validationReport);
				}
			}
			httpQueryManager.treatResponseCode(response, redirAttrs);
		} catch (ShaclValidationException e) {
			rdfManager.treatShaclException(redirAttrs, e.getReport());
		} catch (Exception e) {
			System.out.println(e.toString());
			redirAttrs.addFlashAttribute("error", "La requête n'a pas abouti");
		}
		return "redirect:/";
	}
}