package fr.emse.master.semweb;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Objects;

import static java.time.temporal.ChronoUnit.SECONDS;

public class HttpQueryManager {

    private final HttpClient client;

    public HttpQueryManager() {
        this.client = HttpClient.newBuilder()
                .authenticator(new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                "ldpuser",
                                "LinkedDataIsGreat".toCharArray()
                        );
                    }
                }).build();
    }

    public HttpResponse<String> httpQueryPostSparqlQuery(String uri, String filepath)
            throws URISyntaxException, IOException, InterruptedException {
        return httpQueryPostData(uri, filepath, "application/sparql-query");
    }

    public HttpResponse<String> httpQueryPutRawData(String uri, String data)
            throws URISyntaxException, IOException, InterruptedException {
        return httpQueryPutRawData(uri, data, null);
    }

    public HttpResponse<String> httpQueryPutRawData(String uri, String data, String etag)
            throws URISyntaxException, IOException, InterruptedException {
        return httpQueryPutRawData(uri, data, etag, "text/turtle");
    }

    public HttpResponse<String> httpQueryPutRawData(String uri, String data, String etag, String contentType)
            throws URISyntaxException, IOException, InterruptedException {
        byte[] bodyBytes = data.getBytes(StandardCharsets.UTF_8);
        HttpRequest request;
        if (etag != null) {
            request = HttpRequest.newBuilder()
                    .header("Content-Type", contentType)
                    .header("Accept", "*/*")
                    .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                    .header("If-Match", etag)
                    .timeout(Duration.of(10, SECONDS))
                    .uri(new URI(uri))
                    .PUT(HttpRequest.BodyPublishers.ofByteArray(bodyBytes))
                    .build();
        } else {
            request = HttpRequest.newBuilder()
                    .header("Content-Type", contentType)
                    .header("Accept", "*/*")
                    .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                    .timeout(Duration.of(10, SECONDS))
                    .uri(new URI(uri))
                    .PUT(HttpRequest.BodyPublishers.ofByteArray(bodyBytes))
                    .build();
        }

        return this.client.send(request,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
    }

    public HttpResponse<String> httpQueryPostData(String uri, String filepath)
            throws URISyntaxException, IOException, InterruptedException {
        return httpQueryPostData(uri, filepath, "text/turtle");
    }

    public HttpResponse<String> httpQueryPostRawData(String uri, String data)
            throws URISyntaxException, IOException, InterruptedException {
        return httpQueryPostRawData(uri, data, "text/turtle");
    }

    public HttpResponse<String> httpQueryPostRawData(String uri, String data, String contentType)
            throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", contentType)
                .header("Accept", "*/*")
                .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                .timeout(Duration.of(10, SECONDS))
                .uri(new URI(uri))
                .POST(HttpRequest.BodyPublishers.ofString(data, StandardCharsets.UTF_8))
                .build();
        return this.client.send(request,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
    }


    public HttpResponse<String> httpQueryPostData(String uri, String filepath, String contentType)
            throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", contentType)
                .header("Accept", "*/*")
                .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                .timeout(Duration.of(10, SECONDS))
                .uri(new URI(uri))
                .POST(HttpRequest.BodyPublishers.ofFile(Paths.get(filepath)))
                .build();
        return this.client.send(request,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
    }

    public HttpResponse<String> httpQueryGet(String uri) throws URISyntaxException, IOException, InterruptedException {
//        System.out.println(uri);
        HttpRequest request = HttpRequest.newBuilder()
                .header("Accept", "*/*")
                .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                .timeout(Duration.of(10, SECONDS))
                .uri(new URI(uri))
                .GET()
                .build();
        return this.client.send(request,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
    }

    public HttpResponse<String> httpQueryDelete(String uri)
            throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "text/turtle")
                .header("Accept", "*/*")
                .header("Slug", "Adam_ElBesri_Roman_Guirbal-data")
                .timeout(Duration.of(10, SECONDS))
                .uri(new URI(uri))
                .DELETE()
                .build();
        return this.client.send(request,
                HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
    }

    public void treatResponseCode(HttpResponse<String> response, RedirectAttributes redirAttrs) {
        if (response == null) {
            throw new IllegalArgumentException("Response cannot be null");
        }
        if (Objects.equals(response.request().method().toLowerCase(), "delete")) {
            if (response.statusCode() == 204) {
                redirAttrs.addFlashAttribute("success", "Successfully deleted");
            } else if (response.statusCode() == 404){
                redirAttrs.addFlashAttribute("error", "Resource not found");
            } else {
                System.out.println(response);
                redirAttrs.addFlashAttribute("error", "Platform error (" + response.statusCode() + ")");
            }
        } else if (Objects.equals(response.request().method().toLowerCase(), "post")) {
            if (response.statusCode() == 201) {
                redirAttrs.addFlashAttribute("success", "Successfully added");
            } else if (response.statusCode() == 409) {
                redirAttrs.addFlashAttribute("error", "Conflict, data already exists");
            } else {
                System.out.println(response);
                redirAttrs.addFlashAttribute("error", "Platform error (" + response.statusCode() + ")");
            }
        } else {
            System.out.println(response);
            redirAttrs.addFlashAttribute("error", "Response code " + response.statusCode());
        }
    }
}
