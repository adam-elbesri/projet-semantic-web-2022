package fr.emse.master.semweb;

import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;
import org.apache.jena.shacl.ValidationReport;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RDFManager {

    private final String base;

    public RDFManager() {
        this.base = "https://territoire.emse.fr/ldp/adamelbesriromanguirbal/";
    }

    public Stream<List<String>> getTriples(HttpResponse<String> response) {
        return getTriples(response.body());
    }

    public Stream<List<String>> getTriples(String responseBody) {
        return getTriples(JSON.parse(responseBody));
    }

    public Stream<List<String>> getTriples(JsonObject responseBodyAsJSON) {
        return responseBodyAsJSON
                .getObj("results")
                .getArray("bindings")
                .map((binding) -> Arrays.asList(
                        binding
                                .getAsObject()
                                .getObj("s")
                                .get("value")
                                .getAsString()
                                .value()
                                .replace(base, ""),
                        binding
                                .getAsObject()
                                .getObj("p")
                                .get("value")
                                .getAsString()
                                .value(),
                        binding
                                .getAsObject()
                                .getObj("o")
                                .get("value")
                                .getAsString()
                                .value()
                                .replace(base, "")
                ));
    }

    public List<List<String>> getAllEvents(HttpResponse<String> response) {
        JsonObject responseBodyAsJSON = JSON.parse(response.body());
//        System.out.println(responseBodyAsJSON);
        return responseBodyAsJSON
                .getObj("results")
                .getArray("bindings")
                .map(this::bindingToList)
                .collect(Collectors.toList());
    }

    private List<String> bindingToList(JsonValue binding) {
        List<String> list = new ArrayList<>();
//        list.add("Oral DL");
//        list.add("2023-01-12T10:30:00");
        list.add(0, binding
                .getAsObject()
                .getObj("title")
                .get("value")
                .getAsString()
                .value());
        list.add(1, binding
                .getAsObject()
                .getObj("startDate")
                .get("value")
                .getAsString()
                .value());
        list.add(2, binding
                .getAsObject()
                .getObj("endDate")
                .get("value")
                .getAsString()
                .value());
        list.add(3, binding
                .getAsObject()
                .getObj("location")
                .get("value")
                .getAsString()
                .value());
        list.add(4, binding
                .getAsObject()
                .getObj("description")
                .get("value")
                .getAsString()
                .value());
        list.add(5, binding
                .getAsObject()
                .getObj("eventURI")
                .get("value")
                .getAsString()
                .value());
//        System.out.println(list);
        return list;
    }

    public void treatShaclException(RedirectAttributes redirectAttributes, ValidationReport validationReport) {
        final String[] errStr = {"Contrainte(s) non respectée(s) : "};
        validationReport.getEntries().forEach((reportEntry -> errStr[0] = errStr[0] + reportEntry.message() + " ; "));
        redirectAttributes.addFlashAttribute("error", errStr[0]);
    }
}
