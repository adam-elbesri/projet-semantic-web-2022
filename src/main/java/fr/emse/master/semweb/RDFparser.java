package fr.emse.master.semweb;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.Calendar;
import org.apache.jena.shacl.ShaclValidator;
import org.apache.jena.shacl.Shapes;
import org.apache.jena.shacl.ValidationReport;

public class RDFparser {
    public RDFparser() {}
    public static int parseAndUpload(String path) throws IOException, ParserException, ParseException, URISyntaxException, InterruptedException {
        FileInputStream fin = new FileInputStream(path);
        CalendarBuilder builder = new CalendarBuilder();
        Calendar calendar = builder.build(fin);
        ComponentList eventList;
        eventList = calendar.getComponents();

        for (Object event : eventList) {
            VEvent vEvent = (VEvent) event;
            Boolean isCourse = true;

            String eventURI = vEvent.getProperty("uid").getValue();
            String eventStart = vEvent.getProperty("dtstart").getValue();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = dateFormat.parse(eventStart);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            eventStart= outputFormat.format(date);
            String eventEnd = vEvent.getProperty("dtend").getValue();
            date = dateFormat.parse(eventEnd);
            eventEnd = outputFormat.format(date);
            String eventSummary = vEvent.getProperty("summary").getValue();
            String eventLocation = vEvent.getProperty("location").getValue();
            String eventDescription = vEvent.getProperty("description").getValue();
            eventDescription = eventDescription.replaceAll("\\n", " ");

            Model model = ModelFactory.createDefaultModel();
            Resource rdfEvent = model.createResource("https://territoire.emse.fr/ldp/adamelbesriromanguirbal/event_"+eventURI+"/");
            rdfEvent.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://schema.org/Event"));
            rdfEvent.addProperty(model.createProperty("http://schema.org/name"), eventSummary);
            rdfEvent.addProperty(model.createProperty("http://schema.org/startDate"), model.createTypedLiteral(eventStart, XSDDatatype.XSDdateTime));
            rdfEvent.addProperty(model.createProperty("http://schema.org/endDate"), model.createTypedLiteral(eventEnd, XSDDatatype.XSDdateTime));

            if (eventLocation.toLowerCase().contains("emse"))
            {
                Pattern pattern = Pattern.compile("\\d\\.?\\d{2}+\\w?");
                Matcher matcher = pattern.matcher(eventLocation);
                while (matcher.find()) {
                    String roomNumber = matcher.group(0);
                    roomNumber = roomNumber.replaceAll("\\.", "");
                    String roomFloor = roomNumber.substring(0,1);
                    eventLocation = "https://territoire.emse.fr/kg/emse/fayol/"+roomFloor+"ET/"+roomNumber;
                    if(roomFloor.equals("0")){
                        eventLocation = "https://territoire.emse.fr/kg/emse/fayol/RDC/"+roomNumber;
                    }
                    rdfEvent.addProperty(model.createProperty("http://schema.org/location"), model.createResource(eventLocation));
                }
            }
            else
            {
                rdfEvent.addProperty(model.createProperty("http://schema.org/location"), eventLocation);
            }
            if(isCourse){
                rdfEvent.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://schema.org/Course"));
            }
            rdfEvent.addProperty(model.createProperty("http://schema.org/description"), eventDescription);

            String eventDir = "src/main/resources/events/";
            File dir = new File(eventDir);

            if (!dir.exists()) {
                // Create the directory
                boolean success = dir.mkdirs();
                if (success) {
                    System.out.println("Directory /events created successfully");
                } else {
                    System.out.println("Error creating directory /events");
                }
            }
            // Create a new file in the /events directory for each event
            File file = new File(eventDir + "event_" + eventURI + ".ttl");
            if (!file.exists()) {
                file.createNewFile();
                RDFDataMgr.write(new FileOutputStream(file), model, Lang.TURTLE);
            }

            // validate with SHACL
            FileManager fileManager = new FileManager();
            HttpQueryManager httpQueryManager = new HttpQueryManager();

            Graph dataGraph = RDFDataMgr.loadGraph(file.getPath());
            Shapes shapes = Shapes.parse(fileManager.getDataFilepath("shapes.ttl"));
            ValidationReport validationReport = ShaclValidator.get().validate(shapes, dataGraph);

            if (validationReport.conforms()) {
                httpQueryManager.httpQueryPostData(
                        "https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
                        file.getPath());
            } else {
                file.delete();
                System.out.println("ICAL FILE INVALID SHACL");
                return -1;
//                throw new ShaclValidationException(validationReport);
            }
//            System.out.println(response);

            //RDFDataMgr.write(System.out, model, Lang.TURTLE);
        }
        return 0;
    }
}