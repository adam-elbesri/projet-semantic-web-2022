package fr.emse.master.semweb;

import org.apache.jena.util.ResourceUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.*;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

@Controller
public class LdJsonRetrieverController {

    @PostMapping("/retrieve")
    public String retrieveLdJsonData(@RequestParam("url") String url, Model model) {
        try {
            // Fetch the HTML from the URL
            Document doc = Jsoup.connect(url).get();

            // Select all script tags with type "application/ld+json"
            Elements ldJsonScripts = doc.select("script[type=application/ld+json]");

            String ldJsonData = "";
            // Print out the ld+json data
            for (Element script : ldJsonScripts) {
                JsonObject jsonObject = new JsonParser().parse(script.html()).getAsJsonObject();
                String id = jsonObject.get("@id").getAsString();

                id = "https://territoire.emse.fr/ldp/adamelbesriromanguirbal/alentoor_"+id.replaceAll("[^\\d]", "")+"/";
                jsonObject.addProperty("@id", id);
                String newJson = jsonObject.toString();
                org.apache.jena.rdf.model.Model modelrdf = ModelFactory.createDefaultModel();
                InputStream inputStream = new ByteArrayInputStream(newJson.getBytes(StandardCharsets.UTF_8));
                RDFDataMgr.read(modelrdf, inputStream, Lang.JSONLD);
                RDFDataMgr.write(System.out, modelrdf, Lang.TURTLE);

                StringWriter out = new StringWriter();
                RDFDataMgr.write(out, modelrdf, Lang.TURTLE);
                String turtleString = out.toString();
                HttpQueryManager httpQueryManager;
                httpQueryManager = new HttpQueryManager();
                HttpResponse<String> response;
                response = httpQueryManager.httpQueryPostRawData(
                        "https://territoire.emse.fr/ldp/adamelbesriromanguirbal/",
                        turtleString);
                System.out.println(response);
                //System.out.println(turtleString);

                ldJsonData += turtleString;
            }
            model.addAttribute("ldJsonData", ldJsonData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return "ldjson";
    }
}
