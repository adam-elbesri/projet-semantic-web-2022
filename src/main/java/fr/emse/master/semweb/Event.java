package fr.emse.master.semweb;

import java.time.LocalDateTime;

public class Event {
    private String name;
    private String description;
    private String location;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private String attendee;

    private boolean isCourse;


    public Event() {}

    public String getName() {
        return name;
    }

    public String getAttendee() {return attendee;}

    public boolean getIsCourse() {
        return isCourse;
    }

    public void setIsCourse(boolean course) {
        this.isCourse = course;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAttendee(String attendee) { this.attendee = attendee;}

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }
}
