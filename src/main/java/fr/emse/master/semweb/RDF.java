package fr.emse.master.semweb;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.VCARD;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class RDF {

    public RDF() {}
    public String createRDF(String attendee, Boolean isCourse, String eventURI, String eventSummary, LocalDateTime eventStart, LocalDateTime eventEnd, String eventLocation, String eventDescription ) throws IOException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        Model model = ModelFactory.createDefaultModel();
        Resource rdfEvent = model.createResource("https://territoire.emse.fr/ldp/adamelbesriromanguirbal/event_"+eventURI+"/");
        rdfEvent.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://schema.org/Event"));
        rdfEvent.addProperty(model.createProperty("http://schema.org/name"), eventSummary);
        rdfEvent.addProperty(model.createProperty("http://schema.org/startDate"), model.createTypedLiteral(eventStart.format(formatter), XSDDatatype.XSDdateTime));
        rdfEvent.addProperty(model.createProperty("http://schema.org/endDate"), model.createTypedLiteral(eventEnd.format(formatter), XSDDatatype.XSDdateTime));

        if (eventLocation.toLowerCase().contains("emse"))
        {
            Pattern pattern = Pattern.compile("\\d\\.?\\d{2}+\\w?");
            Matcher matcher = pattern.matcher(eventLocation);
            while (matcher.find()) {
                String roomNumber = matcher.group(0);
                roomNumber = roomNumber.replaceAll("\\.", "");
                String roomFloor = roomNumber.substring(0,1);
                eventLocation = "https://territoire.emse.fr/kg/emse/fayol/"+roomFloor+"ET/"+roomNumber;
                if(roomFloor.equals("0")){
                    eventLocation = "https://territoire.emse.fr/kg/emse/fayol/RDC/"+roomNumber;
                }
                rdfEvent.addProperty(model.createProperty("http://schema.org/location"), model.createResource(eventLocation));
            }
        }
        else
        {
            rdfEvent.addProperty(model.createProperty("http://schema.org/location"), eventLocation);
        }
        if(isCourse){
            rdfEvent.addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://schema.org/Course"));
        }
        if(attendee.length()>0){
            Resource attendeeNode = model.createResource().addProperty(model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), model.createResource("http://schema.org/Person"))
                    .addProperty(model.createProperty("http://schema.org/name"), attendee);
            rdfEvent.addProperty(model.createProperty("http://schema.org/attendee"), attendeeNode);
        }
        rdfEvent.addProperty(model.createProperty("http://schema.org/description"), eventDescription);

        String eventDir = "src/main/resources/temp/";
        File dir = new File(eventDir);

        if (!dir.exists()) {
            // Create the directory
            boolean success = dir.mkdirs();
            if (success) {
                System.out.println("Directory /events created successfully");
            } else {
                System.out.println("Error creating directory /events");
            }
        }
        File file = new File(eventDir + "event_" + eventURI + ".ttl");
        if (!file.exists()) {
            file.createNewFile();
            RDFDataMgr.write(new FileOutputStream(file), model, Lang.TURTLE);
        }

        return file.getPath();
    }
}

