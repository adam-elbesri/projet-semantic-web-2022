# Semantic Web Project 2022

The aim of this file is to help the teacher evaluating our project.
It will describe briefly the project and then list the features we have implemented.

## Team

- Adam El Besri
- Roman Guirbal

## Description

The project, written in Java, is composed of :

- classes that can create, manipulate and query RDF data as well as parsing and transforming iCalendar files into RDF
- a GUI served by a Spring web server.

We used JetBrains IntelliJ IDE to make this project.

To launch the application you need to create a new Java 19.01 project in IntelliJ, import all Maven dependencies and run the file "src/main/java/fr/emse/master/semweb/Semweb2022Application.java"

## Our application

The main page of the application prints a calendar with all the events stored in our LDP container, it also prints a table with all the triples in our container.
You can search for a keyword in all our printed tables.

There 7 actions buttons :
- "Add test event" : To add a default event in LDP
- "Remove test event" : To remove it from the container
- "Upload" : To upload an ical file wich will be parsed and then added to the container
- "My future courses" : To print a table with all events that are Courses
- "Saint Etienne not courses event"  : To print a table with all events that are not courses and are located in "Saint Etienne" 
- "Parse Alentoor events" : To parse an URL from Alentoor and send it events to the LDP container

## Technical resources

To do this project we used :

- Apache Jena to manipulate RDF data
- Jsoup to fetch ld+json data and Google Gson to manipulate json objects
- ical4j to parse ical files
- Spring for the web server
- Thymeleaf as a templating language for our web views
- Territoire LDP as a RDF database to store the data of our calendar and our container

## Features

This part consist in the description of how we answered to all the technical requirements with features.

### Download any ICS file (e.g. the M2 CPS2 time table) and turn it into RDF. The generated RDF graph should include instances of schema:Event and should use identifiers provided by Plateforme Territoire to denote rooms at Espace Fauriel.

To try this feature you can choose an ical file and then click on the "Upload" button, this will parse your ical file and retrieves all its events, then it will create an rdf graph that is an instance of schema:Event, finally it will convert each event into a turtle file and send it to LDP with a POST request.

To use emse identifiers for classrooms we had to parse the property "location" of each event and keep only the numbers in the text wich refers to the room number.
After that we build the identifier link manually considering the extracted room number. For example, the room 121 will be at the first floor and the id 132 so our identifier link will be "https://territoire.emse.fr/kg/emse/fayol/1ET/121".

### Add events to a personal calendar hosted on Plateforme Territoire's LDP. Events can either be generated from an ICS file, extracted from Web pages or manually written.

For this feature, we first created a new Java class Event with the properties of schema.org Event, then we created a basic html where the user can specify all the details about an event (start date, if it is a course, location, if he attended the course or not..)
An rdf graph is created with the properties of the Event object from the form, converted into a Turtle file and sent to LDP with a POST Request.

You can access the form with the link "localhost:8080/add" or the button "Create an event" in the main page of the application.

You can also copy paste the URL of some events in Alentoor after you clicked on "Parse Alentoor events" button or directly on the page "localhost:8080/parsing".
This will get all json+ld data contained in the specific webpage and convert it to an RDF graph as a turtle file wich will be sent to LDP as new events.

Finaly, to generate events from an ICS file you can go back to the first feature.

### List of upcoming events on a given date (e.g. "what are my next classes?"). This feature should include at least one SPARQL query.

We implemented the example given by the teacher wich is "what are my next classes" therefore this list is only printing events that are Courses and that have a startDate greater than actual DateTime.
The SPARQL Query used to retrieve the right events is located in "src/main/resources/data/myFutureCourses.rq" and we use a POST request to send this query

You can access this list with the link "localhost:8080/myfuturecourses" or the button "My future courses" in the main page of the application.

### List events taking place in Saint-Étienne that are not courses. This feature should again use SPARQL.

For this feature we combine the results of two SPARQL queries, the first one retrieves events located in "Saint Etienne" with a location as a literral while the second one is retrieving Alentoor parsed events wich have a more precise location because it's described with a schema:Place so we'are checking that the schema:adressLocality is equals to "Saint Etienne" then we print the result of these two queries in our table. 

The SPARQL Query used to retrieve the right events is located in "src/main/resources/data/steEvents.rq" and we use a POST request to send this query
You can access this list with the link "localhost:8080/steEvents" or the button "Saint Etienne not courses event" in the main page of the application.

### Modify an existing event to indicate that someone has attended it (e.g. "I attended the last SemWeb lecture").

How we did it etc etc
We tried to update an existing event with the method described in the Practical session 

### Validate the information defined for a given event: it should have a start date, an end date, a location, a title, a description, etc. Validation should be done with SHACL.

How we did it etc etc

